using System;
using System.Collections;
using Services.Factory;
using UnityEngine;

namespace Enemy.StateMachine.States
{
    public class DeathState : State<EnemyController>
    {
        private static readonly int _deathTrigger = Animator.StringToHash("DeathTrigger");
        public DeathState(EnemyController core) : base(core) {}

        public override void OnEnter()
        {
            _core.animator.SetTrigger(_deathTrigger);
            _core.StartCoroutine(Delay(3.0f, () => FactoryService.instance.enemy.Release(_core)));
        }

        private IEnumerator Delay(float waitTime, Action action)
        {
            yield return new WaitForSeconds(waitTime);
            action?.Invoke();
        }
    }
}