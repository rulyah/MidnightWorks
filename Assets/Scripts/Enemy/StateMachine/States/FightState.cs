using System;
using System.Collections;
using Player;
using Unity.VisualScripting;
using UnityEngine;

namespace Enemy.StateMachine.States
{
    public class FightState : State<EnemyController>
    {
        private Coroutine _loop;
        private static readonly int _punchTrigger = Animator.StringToHash("PunchTrigger");
        private static readonly int _speed = Animator.StringToHash("Speed");
        public FightState(EnemyController core) : base(core) {}

        public override void OnEnter()
        {
            _core.animator.SetFloat(_speed, 0.0f);
            _loop = _core.StartCoroutine(Loop());
        }

        public override void OnExit()
        {
            _core.StopCoroutine(_loop);
        }

        private IEnumerator Loop()
        {
            while (true)
            {
                var distance = Vector3.Distance(_core.target.position, _core.transform.position);
                if(distance <= _core.config.minDistance)
                {
                    _core.animator.SetTrigger(_punchTrigger);
                    yield return new WaitForSeconds(1.0f);
                    var currentDistance = Vector3.Distance(_core.target.position, _core.transform.position);
                    if(currentDistance <= _core.config.minDistance) _core.target.TakeDamage(_core.config.hitDamage);
                    yield return new WaitForSeconds(3.0f);
                }
                
                if(distance > _core.config.minDistance) ChangeState(new MoveState(_core));
                if(!_core.isAlive) ChangeState(new DeathState(_core));
                yield return null;
            }
        }
    }
}