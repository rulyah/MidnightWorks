using System.Collections;
using UnityEngine;

namespace Enemy.StateMachine.States
{
    public class IdleState : State<EnemyController>
    {
        public IdleState(EnemyController core) : base(core) {}

        private Coroutine _loop;
        private static readonly int _speed = Animator.StringToHash("Speed");

        public override void OnEnter()
        {
            _core.animator.SetFloat(_speed, 0);
            _loop = _core.StartCoroutine(Loop());
        }

        public override void OnExit()
        {
            _core.StopCoroutine(_loop);
        }

        private IEnumerator Loop()
        {
            while (true)
            {
                yield return null;
                var distance = Vector3.Distance(_core.target.position, _core.transform.position);
                if(distance <= _core.config.maxDistance && distance > _core.config.minDistance) ChangeState(new MoveState(_core));
                if(distance <= _core.config.minDistance) ChangeState(new FightState(_core));
                if(!_core.isAlive) ChangeState(new DeathState(_core));
            }
        }
    }
}