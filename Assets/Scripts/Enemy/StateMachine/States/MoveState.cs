using System.Collections;
using UnityEngine;

namespace Enemy.StateMachine.States
{
    public class MoveState : State<EnemyController>
    {
        private static readonly int _speed = Animator.StringToHash("Speed");
        private Coroutine _loop;
        public MoveState(EnemyController core) : base(core) {}

        public override void OnEnter()
        {
            _core.animator.SetFloat(_speed, 1);
            _loop = _core.StartCoroutine(Loop());
        }

        public override void OnExit()
        {
            _core.StopCoroutine(_loop);
        }

        private IEnumerator Loop()
        {
            while (true)
            {
                var distance = Vector3.Distance(_core.target.position, _core.transform.position);
                if (distance <= _core.config.maxDistance && distance > _core.config.minDistance)
                {
                    var moveDirection = _core.target.position - _core.transform.position;
                    _core.transform.forward = moveDirection.normalized;
                    _core.transform.position += moveDirection * (_core.config.moveSpeed * Time.deltaTime);
                }
                if(distance > _core.config.maxDistance) ChangeState(new IdleState(_core));
                if(distance <= _core.config.minDistance) ChangeState(new FightState(_core));
                if(!_core.isAlive) ChangeState(new DeathState(_core));
                yield return null;
            }
        }
    }
}