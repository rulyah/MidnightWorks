using System;
using Cinemachine;
using Configs;
using Enemy.StateMachine;
using Enemy.StateMachine.States;
using UI;
using UnityEngine;
using Utils.FactoryTool;
using Utils.Process;

namespace Enemy
{
    public class EnemyController : PoolableMonoBehaviour, IALive, ICoroutineRunner
    {
        [SerializeField] private HealthBar _healthBar;
        public EnemyConfig config;
        public Animator animator;

        public Vector3 position => transform.position;
        public IALive target;
        public bool isAlive = true;
        
        
        private EnemyModel _model;
        private StateMachine<EnemyController> _stateMachine;

        public event Action<EnemyController> onDeath;

        public void Init(IALive target, CameraController camera)
        {
            this.target = target;
            _model = new EnemyModel()
            {
                health = config.maxHealth
            };
            _healthBar.Init(camera);
            _healthBar.ChangeHealth(_model.health, config.maxHealth);
            _stateMachine = new StateMachine<EnemyController>(new IdleState(this));
        }
        
        public void TakeDamage(int value)
        {
            if(!isAlive) return;
            _model.health -= value;
            _model.health = Mathf.Clamp(_model.health, 0, int.MaxValue);
            _healthBar.ChangeHealth(_model.health, config.maxHealth);
            if (_model.health <= 0)
            {
                isAlive = false;
                onDeath?.Invoke(this);
            }
        }
    }
}