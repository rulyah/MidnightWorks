using Configs;
using UnityEngine;
using Utils.FactoryTool;

namespace Weapons
{
    public class Weapon : PoolableMonoBehaviour
    {
        public WeaponConfig config;
        public Transform shootPoint;
        public ParticleSystem muzzleFlash;
        public AudioSource audioSource;
    }
}