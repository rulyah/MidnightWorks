using Enemy;
using Player;
using UnityEngine;
using Utils.Process;

namespace Processes
{
    public class PlayerFireProcess : Process
    {
        private PlayerController _player;
        private float _raycastDistance = 50.0f;
        private float _nextFire;

        public PlayerFireProcess(PlayerController player) : base(player)
        {
            _player = player;
        }

        private void Shoot()
        {
            _player.currentWeapon.muzzleFlash.Play();
            _player.currentWeapon.audioSource.Play();
            
            RaycastHit hit;
            if (Physics.Raycast(_player.currentWeapon.shootPoint.position, 
                    _player.transform.forward, out hit, _raycastDistance))
            {
                if (hit.collider.gameObject.GetComponent<IALive>() is EnemyController enemy)
                {
                    Debug.Log("Hit ENEMY!!!!");
                    enemy.TakeDamage(_player.currentWeapon.config.damage);
                }
            }
        }

        protected override void OnUpdate()
        {
            if (Input.GetMouseButton(0) && Time.time >= _nextFire)
            {
                Debug.Log("Fire");
                _nextFire = Time.time + 1.0f / _player.currentWeapon.config.fireRate;
                Shoot();
            }
        }
    }
}