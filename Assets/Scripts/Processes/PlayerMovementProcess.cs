using Player;
using UnityEngine;
using Utils.Process;

namespace Processes
{
    public class PlayerMovementProcess : Process
    {
        private PlayerController _player;
        private float _currentSpeed;
        private float _rotY;

        private static readonly int _speed = Animator.StringToHash("Speed");

        public PlayerMovementProcess(PlayerController player) : base(player)
        {
            _player = player;
            _currentSpeed = _player.config.moveSpeed;
        }

        protected override void OnUpdate()
        {
            
            
            
            var moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).normalized;
            if (moveDirection.magnitude > 0.0f)
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    _currentSpeed = _player.config.runSpeed;
                    _player.animator.SetFloat(_speed, 2);
                }
                else
                {
                    _currentSpeed = _player.config.moveSpeed;
                    _player.animator.SetFloat(_speed, 1);
                }
                
                moveDirection = _player.transform.TransformDirection(moveDirection);
                _player.transform.position += moveDirection * (_currentSpeed * Time.deltaTime);
            }
            else
            {
                _currentSpeed = 0.0f;
                _player.animator.SetFloat(_speed, 0.0f);
            }
        }
    }
}