using Player;
using UnityEngine;
using Utils.Process;

namespace Processes
{
    public class PlayerRotationProcess : Process
    {
        private PlayerController _player;
        private float _rotY;

        
        public PlayerRotationProcess(PlayerController player) : base(player)
        {
            _player = player;
        }

        protected override void OnUpdate()
        {
            _rotY += Input.GetAxis("Mouse X") * 2.0f;
            _player.transform.rotation = Quaternion.Euler( 0.0f, _rotY, 0.0f);
        }
    }
}