using System.Collections.Generic;
using Enemy;
using Player;
using UnityEngine;
using Utils.FactoryTool;
using Weapons;

namespace Services.Factory
{
    public class FactoryService : MonoBehaviour
    {
        public static FactoryService instance { get; private set; }
        
        [SerializeField] private PlayerController _playerPrefab;
        [SerializeField] private EnemyController _enemyPrefab;
        [SerializeField] private List<Weapon> _weaponsPrefabs;
        
        public Factory<PlayerController> player { get; private set; }
        public Factory<EnemyController> enemy { get; private set; }
        public List<Factory<Weapon>> weapons { get; private set; }



        private void Awake()
        {
            instance = this;
            
            player = new Factory<PlayerController>(_playerPrefab, 1);
            enemy = new Factory<EnemyController>(_enemyPrefab, 15);
            weapons = new List<Factory<Weapon>>(_weaponsPrefabs.Count);
            
            for (var i = 0; i < _weaponsPrefabs.Count; i++)
            {
                var weapon = new Factory<Weapon>(_weaponsPrefabs[i], 1);
                weapons.Add(weapon);
            }
        }

        private void OnDestroy()
        {
            player.Dispose();
            enemy.Dispose();
            foreach (var weapon in weapons)
            {
                weapon.Dispose();
            }
        }
    }
}