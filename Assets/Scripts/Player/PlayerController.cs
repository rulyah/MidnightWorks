using System;
using System.Collections.Generic;
using Cinemachine;
using Configs;
using Player.StateMachine;
using Player.StateMachine.States;
using Processes;
using UnityEngine;
using Utils.FactoryTool;
using Utils.Process;
using Weapons;

namespace Player
{
    public class PlayerController : PoolableMonoBehaviour, IALive, ICoroutineRunner
    {
        public PlayerConfig config;
        public CapsuleCollider collider;
        public Animator animator;
        public Rigidbody rigidbody;
        public Transform weaponTransform;
        public GameObject viewPoint;
        public CinemachineVirtualCamera _camera;

        public Weapon currentWeapon;
        public bool isAlive = true;
        public bool canChangeWeapon = true;
        public Vector3 position => transform.position;

        private PlayerModel _model;
        
        private StateMachine<PlayerController> _stateMachine;
        public Dictionary<Type, Process> processes;

        public event Action onPlayerDeath;
        
        public void Init(int weaponId)
        {
            processes = new Dictionary<Type, Process>();
            processes.Add(typeof(PlayerMovementProcess), new PlayerMovementProcess(this));
            processes.Add(typeof(PlayerFireProcess), new PlayerFireProcess(this));
            processes.Add(typeof(PlayerRotationProcess), new PlayerRotationProcess(this));
            
            _model = new PlayerModel
            {
                health = config.maxHealth
            };
            _stateMachine = new StateMachine<PlayerController>(new SetWeaponState(this, weaponId));
        }

        public bool TryGetProcess<T>(out T process) where T : Process
        {
            if (processes.ContainsKey(typeof(T)))
            {
                process = (T)processes[typeof(T)];
                return true;
            }

            process = null;
            return false;
        }

        public void TryStartProcess<T>() where T : Process
        {
            if (TryGetProcess(out T process))
            {
                if (!process.isRunning) process.Start();
            } 
        }

        public void TryStopProcess<T>() where T : Process
        {
            if (TryGetProcess(out T process))
            {
                if(process.isRunning) process.Stop();
            }
        }
        
        public void TakeDamage(int value)
        {
            if(!isAlive) return;
            _model.health -= value;
            if (_model.health <= 0)
            {
                isAlive = false;
                onPlayerDeath?.Invoke();
            }
        }

        public void ChangeWeapon(int weaponId)
        {
            _stateMachine.ChangeState(new SetWeaponState(this, weaponId));
        }

        private void Update()
        {
            _camera.Priority = Input.GetMouseButton(1) ? 11 : 0;
        }
    }
}