using System.Collections;
using Commands;
using Processes;
using UnityEngine;

namespace Player.StateMachine.States
{
    public class MoveState : State<PlayerController>
    {
        public MoveState(PlayerController core) : base(core) {}
        private Coroutine _loop;
        private static readonly int _jumpTrigger = Animator.StringToHash("JumpTrigger");

        public override void OnEnter()
        {
            _core.TryStartProcess<PlayerMovementProcess>();
            _loop = _core.StartCoroutine(Loop());
        }

        public override void OnExit()
        {
            _core.StopCoroutine(_loop);
        }

        private IEnumerator Loop()
        {
            while (true)
            {
                var moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).normalized;
                if (moveDirection.magnitude <= 0.01f)
                {
                    ChangeState(new IdleState(_core));
                }

                else if (Input.GetKeyDown(KeyCode.Space) && !CheckFallingCommand.Execute(_core))
                {
                    _core.animator.SetTrigger(_jumpTrigger);
                    _core.rigidbody.AddForce(Vector3.up * _core.config.jumpForce, ForceMode.Impulse);
                    ChangeState(new FallState(_core));
                }
                if(!_core.isAlive) ChangeState(new DeathState(_core));
                yield return null;
            }
        }
    }
}