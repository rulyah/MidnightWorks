using Processes;
using UnityEngine;

namespace Player.StateMachine.States
{
    public class DeathState: State<PlayerController>
    {
        private static readonly int _deathTrigger = Animator.StringToHash("DeathTrigger");
        public DeathState(PlayerController core) : base(core) {}

        public override void OnEnter()
        {
            SavesManager.AddLose();
            _core.canChangeWeapon = false;
            _core.TryStopProcess<PlayerMovementProcess>();
            _core.TryStopProcess<PlayerFireProcess>();
            _core.TryStopProcess<PlayerRotationProcess>();
            _core.animator.SetTrigger(_deathTrigger);
        }
    }
}