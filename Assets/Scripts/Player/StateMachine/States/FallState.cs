using System.Collections;
using Commands;
using Processes;
using UnityEngine;

namespace Player.StateMachine.States
{
    public class FallState : State<PlayerController>
    {
        public FallState(PlayerController core) : base(core) {}

        private Coroutine _loop;
        public override void OnEnter()
        {
            _core.TryStopProcess<PlayerMovementProcess>();
            _loop = _core.StartCoroutine(Loop());
        }

        public override void OnExit()
        {
            _core.StopCoroutine(_loop);
        }

        private IEnumerator Loop()
        {
            while (true)
            {
                yield return null;
                if (!CheckFallingCommand.Execute(_core))
                {
                    var moveDirection = new Vector3(
                        Input.GetAxis("Horizontal"), 
                        0.0f, 
                        Input.GetAxis("Vertical")).normalized;
                    if(moveDirection.magnitude <= 0.1f) ChangeState(new IdleState(_core));
                    ChangeState(new MoveState(_core));
                }
                if(!_core.isAlive) ChangeState(new DeathState(_core));
            }
        }
    }
}