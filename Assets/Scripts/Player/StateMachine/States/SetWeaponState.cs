using System;
using System.Collections;
using Processes;
using Services.Factory;
using UI.Screens;
using UnityEngine;

namespace Player.StateMachine.States
{
    public class SetWeaponState : State<PlayerController>
    {
        private static readonly int _putBackTrigger = Animator.StringToHash("PutBackTrigger");
        private static readonly int _pullOutTrigger = Animator.StringToHash("PullOutTrigger");
        private int _weaponId;

        public SetWeaponState(PlayerController core, int weaponId) : base(core)
        {
            _weaponId = weaponId;
        }

        public override void OnEnter()
        {
            _core.canChangeWeapon = false;
            _core.TryStopProcess<PlayerRotationProcess>();
            _core.TryStopProcess<PlayerMovementProcess>();
            _core.TryStopProcess<PlayerFireProcess>();

            if (_core.currentWeapon != null)
            {
                _core.animator.SetTrigger(_putBackTrigger);
                _core.StartCoroutine(Delay(1.2f, () =>
                {
                    FactoryService.instance.weapons[_core.currentWeapon.config.id].Release(_core.currentWeapon);
                    _core.currentWeapon = null;
                    _core.StartCoroutine(Delay(1.0f, () => EquipWeapon(_weaponId)));
                }));
            }
            else
            {
                EquipWeapon(_weaponId);
            }
        }

        public override void OnExit()
        {
            _core.TryStartProcess<PlayerFireProcess>();
            _core.TryStartProcess<PlayerRotationProcess>();
            _core.canChangeWeapon = true;
        }

        private void EquipWeapon(int id)
        {
            _core.animator.SetTrigger(_pullOutTrigger);
            _core.StartCoroutine(Delay(0.5f, () =>
            {
                var weapon = FactoryService.instance.weapons[id].Produce();
                weapon.transform.SetParent(_core.weaponTransform);
                weapon.transform.position = _core.weaponTransform.position;
                weapon.transform.localRotation = Quaternion.identity;
                _core.currentWeapon = weapon;

                _core.StartCoroutine(Delay(2.0f, () =>
                {
                    ChangeState(new IdleState(_core));
                }));
            }));
        }
        
        private IEnumerator Delay(float waitTime, Action action)
        {
            yield return new WaitForSeconds(waitTime);
            action?.Invoke();
        }
    }
}