using UnityEngine;

public interface IALive
{
    public Vector3 position { get; }

    public void TakeDamage(int value);
}