using UnityEngine;

public static class SavesManager
{
    private const string LOSS = "loss_count";
    private const string VICTORY = "victory_count";


    public static void AddLose()
    {
        var count = GetLoseCount() + 1;
        PlayerPrefs.SetInt(LOSS, count);
    }

    public static void AddVictory()
    {
        var count = GetVictoryCount() + 1;
        PlayerPrefs.SetInt(VICTORY, count);
    }

    public static int GetLoseCount()
    {
        return PlayerPrefs.GetInt(LOSS, 0);
    }

    public static int GetVictoryCount()
    {
        return PlayerPrefs.GetInt(VICTORY, 0);
    }
}