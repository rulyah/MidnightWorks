using System;
using System.Collections.Generic;
using Enemy;
using Player;
using Processes;
using Services.Factory;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Core : MonoBehaviour
{
    [SerializeField] private CameraController _camera;
    [SerializeField] private ScreenController _screenController;
    
    private PlayerController _player;
    private List<EnemyController> _enemies;
    private int _enemyCount = 10;
    private bool _isPlayerDeath;
    private void Start()
    {
        _screenController.menu.onPlay += OnGameStart;
        _screenController.menu.onExit += OnGameExit;
        _screenController.equipment.onWeaponSelect += ChangeWeapon;
        _screenController.equipment.onClose += OnEquipmentClose;
        _screenController.loss.onRestart += OnRestart;
        _screenController.victory.onRestart += OnRestart;
    }

    private void OnDestroy()
    {
        _screenController.menu.onPlay -= OnGameStart;
        _screenController.menu.onExit -= OnGameExit;
        _screenController.equipment.onWeaponSelect -= ChangeWeapon;
        _screenController.equipment.onClose -= OnEquipmentClose;
        _screenController.loss.onRestart -= OnRestart;
        _screenController.victory.onRestart -= OnRestart;
    }

    private void OnRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnEquipmentClose()
    {
        Time.timeScale = 1.0f;
        _screenController.CloseLastScreen();
        _player.TryStopProcess<PlayerFireProcess>();
        _player.TryStopProcess<PlayerRotationProcess>();
    }

    private void ChangeWeapon(int weaponId)
    {
        _player.ChangeWeapon(weaponId);
        OnEquipmentClose();
    }
    
    private void OnGameStart()
    {
        _player = FactoryService.instance.player.Produce();
        var weaponId = Random.Range(0, FactoryService.instance.weapons.Count);
        _player.Init(weaponId);
        _player.onPlayerDeath += OnPlayerDeath;

        _enemies = new List<EnemyController>();
        
        SetEnemy();
        _camera.Init(_player, _player.viewPoint);
        _screenController.CloseLastScreen();
        _screenController.OpenScreen(_screenController.game);
    }

    private void SetEnemy()
    {
        for (var i = 0; i < _enemyCount; i++)
        {
            var enemy = FactoryService.instance.enemy.Produce();
            enemy.transform.position = Random.insideUnitSphere * 50.0f;
            enemy.transform.position = new Vector3(enemy.transform.position.x,
                0.0f, enemy.transform.position.z);
            enemy.Init(_player, _camera);
            enemy.onDeath += OnEnemyDeath;
            _enemies.Add(enemy);
        }
    }

    private void OnEnemyDeath(EnemyController enemy)
    {
        enemy.onDeath -= OnEnemyDeath;
        _enemies.Remove(enemy);
        if (_enemies.Count <= 0)
        {
            SavesManager.AddVictory();
            _screenController.CloseLastScreen();
            _screenController.OpenScreen(_screenController.victory);
        }
    }
    
    private void OnPlayerDeath()
    {
        _isPlayerDeath = true;
    }

    private void OnGameExit()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B) && _player.canChangeWeapon)
        {
            _player.TryStopProcess<PlayerFireProcess>();
            _player.TryStopProcess<PlayerRotationProcess>();
            Time.timeScale = 0;
            _screenController.equipment.SetCurrentWeaponId(_player.currentWeapon.config.id);
            _screenController.OpenScreen(_screenController.equipment);
        }

        if (_isPlayerDeath)
        {
            if (Input.anyKeyDown)
            {
                _screenController.CloseLastScreen();
                _screenController.OpenScreen(_screenController.loss);
            }
        }
        
        
    }
}
