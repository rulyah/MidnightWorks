using Cinemachine;
using Player;
using UnityEngine;
using Utils.Process;

public class CameraController : MonoBehaviour, ICoroutineRunner
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    
    public void Init(PlayerController target, GameObject lookTarget)
    {
        _camera.Follow = target.transform;
        _camera.LookAt = lookTarget.transform;
    }
    
}