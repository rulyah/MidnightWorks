using Player;
using UnityEngine;

namespace Commands
{
    public static class CheckFallingCommand
    {
        public static bool Execute(PlayerController player)
        {
            Ray ray = new Ray(new Vector3(player.transform.position.x, 
                player.transform.position.y + player.collider.radius, 
                player.transform.position.z), -player.transform.up);
            return Physics.SphereCast(ray, player.collider.radius, 0.01f);
        }
    }
}