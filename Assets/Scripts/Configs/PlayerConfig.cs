using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(menuName = "Configs/Player", fileName = "PlayerConfig")]
    public class PlayerConfig : ScriptableObject
    {
        public float moveSpeed;
        public float runSpeed;
        public int maxHealth;
        public float jumpForce;
    }
}