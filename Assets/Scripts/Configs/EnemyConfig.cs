using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(menuName = "Configs/Enemy", fileName = "EnemyConfig")]
    public class EnemyConfig : ScriptableObject
    {
        public float moveSpeed;
        public float maxDistance;
        public float minDistance;
        public int maxHealth;
        public int hitDamage;
    }
}