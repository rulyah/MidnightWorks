using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(menuName = "Configs/Weapon", fileName = "WeaponConfig")]
    public class WeaponConfig : ScriptableObject
    {
        public int id;
        public int damage;
        public float fireRate;
    }
}