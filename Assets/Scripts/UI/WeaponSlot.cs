using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WeaponSlot : MonoBehaviour
    {
        [SerializeField] private Button _selectBtn;
        [SerializeField] private int _weaponId;

        public event Action<int> onWeaponSelect;

        public void Init()
        {
            _selectBtn.onClick.AddListener(OnSelect);
        }

        public void DeInit()
        {
            _selectBtn.onClick.RemoveListener(OnSelect);
        }

        private void OnSelect()
        {
            onWeaponSelect?.Invoke(_weaponId);
        }
    }
}