using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Screens
{
    public class VictoryScreen : Screen
    {
        [SerializeField] private Button _restartBtn;

        public event Action onRestart;
        
        public override void Show()
        {
            base.Show();
            _restartBtn.onClick.AddListener(OnNextClick);
        }

        public override void Hide()
        {
            _restartBtn.onClick.RemoveListener(OnNextClick);
            base.Hide();
        }

        private void OnNextClick()
        {
            onRestart?.Invoke();
        }
    }
}