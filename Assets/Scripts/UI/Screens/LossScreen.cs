using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Screens
{
    public class LossScreen : Screen
    {
        [SerializeField] private Button _restartBtn;

        public event Action onRestart;
        
        public override void Show()
        {
            base.Show();
            _restartBtn.onClick.AddListener(OnRestartClick);
        }

        public override void Hide()
        {
            _restartBtn.onClick.RemoveListener(OnRestartClick);
            base.Hide();
        }

        private void OnRestartClick()
        {
            onRestart?.Invoke();
        }
    }
}