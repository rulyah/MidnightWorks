using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Screens
{
    public class MenuScreen : Screen
    {
        [SerializeField] private Button _playBtn;
        [SerializeField] private Button _exitBtn;
        [SerializeField] private TextMeshProUGUI _loseText;
        [SerializeField] private TextMeshProUGUI _victoryText;


        public event Action onPlay;
        public event Action onExit;
        
        public override void Show()
        {
            base.Show();
            _loseText.text = $"Lose count: {SavesManager.GetLoseCount().ToString()}";
            _victoryText.text = $"Victory count: {SavesManager.GetVictoryCount().ToString()}";
            _playBtn.onClick.AddListener(OnPlayClick);
            _exitBtn.onClick.AddListener(OnExitClick);
        }

        public override void Hide()
        {
            _playBtn.onClick.RemoveListener(OnPlayClick);
            _exitBtn.onClick.RemoveListener(OnExitClick);
            base.Hide();
        }

        private void OnPlayClick()
        {
            onPlay?.Invoke();
        }

        private void OnExitClick()
        {
            onExit?.Invoke();
        }
    }
}