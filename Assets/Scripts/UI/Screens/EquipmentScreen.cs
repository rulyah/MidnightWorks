using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Weapons;

namespace UI.Screens
{
    public class EquipmentScreen : Screen
    {
        [SerializeField] private List<WeaponSlot> _slots;
        [SerializeField] private Button _closeBtn;
        private int _currentWeaponId;

        public event Action<int> onWeaponSelect;
        public event Action onClose; 
        
        public override void Show()
        {
            base.Show();
            foreach (var slot in _slots)
            {
                slot.Init();
                slot.onWeaponSelect += OnWeaponSelect;
            }
            _slots[_currentWeaponId].gameObject.SetActive(false);
            _closeBtn.onClick.AddListener(OnClose);
        }

        public void SetCurrentWeaponId(int weaponId)
        {
            _currentWeaponId = weaponId;
        }

        private void OnWeaponSelect(int weaponId)
        {
            onWeaponSelect?.Invoke(weaponId);
        }

        private void OnClose()
        {
            onClose?.Invoke();
        }

        public override void Hide()
        {
            _slots[_currentWeaponId].gameObject.SetActive(true);
            foreach (var slot in _slots)
            {
                slot.DeInit();
                slot.onWeaponSelect -= OnWeaponSelect;
            }
            base.Hide();
        }
    }
}