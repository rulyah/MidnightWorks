using System.Collections.Generic;
using UI.Screens;
using UnityEngine;

namespace UI
{
    public class ScreenController : MonoBehaviour
    {
        public MenuScreen menu;
        public GameScreen game;
        public EquipmentScreen equipment;
        public LossScreen loss;
        public VictoryScreen victory;

        private Stack<Screen> _screens;
        
        private void Awake()
        {
            _screens = new Stack<Screen>();
            OpenScreen(menu);
        }
        
        public void OpenScreen(Screen screen)
        {
            _screens.Push(screen);
            screen.Show();
        }

        public void CloseLastScreen()
        {
            if(_screens.Count > 0) _screens.Pop().Hide();
        }
        
    }
}